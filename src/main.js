import {createApp, onMounted} from 'vue'
import './assets/css/style.css'
import App from './App.vue'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap/dist/js/bootstrap.min.js'

import 'aos/dist/aos.css'
import axios from "axios";
import AOS from "aos";
import router from "./router/router.js";

createApp(App).use(router).mount('#app')

onMounted(() => {
    AOS.init({})
})

axios.interceptors.response.use(
    (response) => {
        return response
    },
    (error) => {
        console.log(error);
    }
);

import $ from "jquery";

export default $;
