import {createRouter, createWebHistory} from 'vue-router'
import Home from "../view/home/index.vue";
import Sorry from "../view/sorry/index.vue";
import Result from "../view/result/index.vue";

const routes = [
    {
        id: 1,
        path: '/',
        component: Home,
        meta: {title: 'Home'}
    },
    {
        id: 2,
        path: '/sorry-bae',
        name: 'Sorry',
        component: Sorry,
        meta: {title: 'Sorry bae'}
    },
    {
        id: 3,
        path: '/result',
        name: 'Result',
        component: Result,
        meta: {title: 'Result'}
    }
]

const router = createRouter({
    history: createWebHistory(),
    routes,
})

export default router;